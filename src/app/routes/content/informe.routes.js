'use strict';

'use strict';

var express = require('express');
var InformeController = require('../../controllers/informe.controller');
var md_auth = require('../../../midleware/auth.midleware');
var api = express.Router();

api.post('/informe', md_auth.ensureAuth, InformeController.CrearInforme);
api.get(
    '/informe',
    md_auth.ensureAuth,
    InformeController.ListarInformesPorUsuario
);
api.put('/informe', md_auth.ensureAuth, InformeController.ActualizarInforme);
api.delete('/informe', md_auth.ensureAuth, InformeController.EliminarInforme);

module.exports = api;
