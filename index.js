'use strict';

var mongoose = require('mongoose');
var app = require('./app');
var port = process.env.PORT || 8000;
var eventos = require('events');
var Socket = require('socket.io');
// var socketEvent = require('./src/config/socket')

//CONEXION A LA BASE DE DATOS
mongoose.connect(
    'mongodb://127.0.0.1:27017/hroman_db',
    { useNewUrlParser: true },
    (err, res) => {
        if (err) {
            throw err;
        } else {
            console.log('Conexion a la base de datos establecida');
            var server = app.listen(port, function() {
                console.log(
                    'Servidor escuchando por el puerto http://localhost:' + port
                );
            });

            /*  Inicializacion de sokets
             *   var io = require('socket.io')(server)
             *   var socket = new socketEvent(io)
             *   socket.socketEvents();
             * */
        }
    }
);
