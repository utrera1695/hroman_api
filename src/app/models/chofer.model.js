'use stric';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ChoferSchema = Schema({
    nombre: String,
    apellido: String
});

module.exports = mongoose.model('Chofer', ChoferSchema);
