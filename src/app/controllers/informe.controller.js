'use strict';

const Informe = require('../models/informe.model');

async function CrearInforme(req, res) {
    const informe = new Informe(req.body);
    try {
        const newInforme = await informe.save();
        if (newInforme) {
            res.status(200).send({ informe: informe });
        } else {
            res.status(400).send({ error: 'No se pudo crear el informe' });
        }
    } catch (error) {
        res.status(500).send({ error: error });
    }
}

async function ListarInformesPorUsuario(req, res) {
    const user = req.query.user;
    try {
        const informes = await Informe.find({ user: user });
        if (informes) {
            res.status(200).send({ informes: informes });
        } else {
            res.status(400).send({ error: 'No existen informes' });
        }
    } catch (error) {
        res.status(500).send({ error: error });
    }
}

async function EliminarInforme(req, res) {
    const id = req.query.id;
    try {
        const informe = await Informe.findByIdAndRemove(id);
        if (informe) {
            res.status(200).send({ informe: informe });
        } else {
            res.status(400).send({ error: 'No existen informes' });
        }
    } catch (error) {
        res.status(500).send({ error: error });
    }
}

async function ActualizarInforme(req, res) {
    const id = req.query.id;
    const update = req.body;
    try {
        const informe = await Informe.findByIdAndUpdate(id, update);
        if (informe) {
            res.status(200).send({ informe: informe });
        } else {
            res.status(400).send({ error: 'No existen informes' });
        }
    } catch (error) {
        res.status(500).send({ error: error });
    }
}

module.exports = {
    CrearInforme,
    ListarInformesPorUsuario,
    EliminarInforme,
    ActualizarInforme
};
