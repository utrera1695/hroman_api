'use strict';

const Chofer = require('../models/chofer.model');

async function CreateChofer(req, res) {
    try {
        const chofer = new Chofer(req.body);
        await chofer.save();
        res.status(200).send({ chofer: chofer });
    } catch (e) {
        res.status(400).send({ error: e });
    }
}

async function DeleteChofer(req, res) {
    try {
        const chofer = await Chofer.findByIdAndDelete(req.query.id).exec();
        res.status(200).send({ chofer: chofer });
    } catch (e) {
        res.status(400).send({ error: e });
    }
}

async function UpdateChofer(req, res) {
    try {
        const chofer = await Chofer.findByIdAndUpdate(req.query.id, req.body).exec();
        res.status(200).send({ chofer: chofer });
    } catch (e) {
        res.status(200).send({ error: e });
    }
}

async function GetAllChofer(req, res) {
    try {
        const choferes = await Chofer.find().exec();
        res.status(200).send({ choferes: choferes });
    } catch (e) {
        res.status(200).send({ error: e });
    }
}

module.exports = {
    CreateChofer,
    DeleteChofer,
    UpdateChofer,
    GetAllChofer
};
