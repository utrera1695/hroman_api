'use strict';
var user = require('./content/user.routes');
var informe = require('./content/informe.routes');
var chofer = require('./content/chofer.routes');
module.exports = {
    user,
    informe,
    chofer
};
