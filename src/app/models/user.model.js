'use stric';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = Schema({
    email: String,
    password: String,
    passhash: String,
    role: String,
    firstname: String,
    lastname: String,
    phone: { type: String, default: 'none' }
});

module.exports = mongoose.model('User', UserSchema);
