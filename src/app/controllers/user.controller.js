'use strict';

var bcrypt = require('bcrypt');
var User = require('../models/user.model');
var jwt = require('../../config/jwt');
var jwt2 = require('jwt-simple');
var secret = 'clave_secreta';
// var mailer = require('./mail.controller')

async function SaveUser(req, res) {
    var params = req.body;
    console.log(params);
    var user = new User(params);
    try {
        //BUSCA SI EXISTE UN USUARIO CON EL MMISMO NOMBRE EN LA BASE DE DATOS
        const users = await User.findOne({ email: user.email }).exec();
        if (users === null) {
            user.passhash = await bcrypt.hash(params.password, 10);
            const newUser = await user.save();
            if (newUser) {
                res.status(200).send({ status: true, user: newUser });
            } else {
                res.status(404).send({
                    message: 'No se ha podido guardar los datos del usuario'
                });
            }
        } else {
            res.status(400).send({
                status: false,
                message: 'Ya existe el usuario'
            });
        }
    } catch (error) {
        res.status(500).send({ error: error });
    }
}

async function LoginUser(req, res) {
    var params = req.body;
    console.log(params);
    var email = params.email;
    var password = params.password;

    try {
        const user = await User.findOne({ email: email }).exec();
        if (user) {
            const compareHash = await bcrypt.compareSync(
                password,
                user.passhash
            );
            if (compareHash) {
                res.status(200).send({
                    token: jwt.createToken(user),
                    role: user.role
                });
            } else {
                res.status(400).send({ message: 'Contraseña erronea' });
            }
        } else {
            res.status(400).send({ message: 'EL usuario no existe' });
        }
    } catch (error) {
        res.status(500).send({ error: error });
    }
}

async function UpdateUser(req, res) {
    const update = req.body;
    const id = req.query.id;
    try {
        if (update.password !== '') {
            update.pass = update.password;
            update.passhash = await bcrypt.hashSync(update.pass, 10);
        }
        const userUpdate = await User.findByIdAndUpdate(id, update).exec();
        if (userUpdate) {
            res.status(200).send({ status: true, user: userUpdate });
        } else {
            res.status(400).send({ message: 'EL usuario no existe' });
        }
    } catch (error) {
        res.status(500).send({ error: error });
    }
}

async function DeleteUser(req, res) {
    const id = req.query.id;
    try {
        const userDelete = await User.findByIdAndDelete(id).exec();
        if (userDelete) {
            res.status(200).send({ status: true, user: userDelete });
        } else {
            res.status(400).send({ message: 'EL usuario no existe' });
        }
    } catch (error) {
        res.status(500).send({ error: error });
    }
}

async function ListUsers(req, res) {
    const role = req.query.role;
    try {
        const users = await User.find({ role: role }).exec();
        if (users) {
            res.status(200).send({ status: true, users: users });
        } else {
            res.status(400).send({ message: 'No se pudo listar los usuarios' });
        }
    } catch (error) {
        res.status(500).send({ error: error });
    }
}

async function ShowMyUser(req, res) {
    try {
        var token = req.headers.authorization.replace(/['"]+/g, '');
        const payload = await jwt2.decode(token, secret);
        const id = payload.sub;
        console.log(payload);
        const user = await User.findById(id).exec();
        if (user) {
            res.status(200).send({ status: true, user: user });
        } else {
            res.status(400).send({ message: 'EL usuario no existe' });
        }
    } catch (error) {
        res.status(500).send({ error: error });
    }
}

module.exports = {
    SaveUser,
    LoginUser,
    UpdateUser,
    DeleteUser,
    ListUsers,
    ShowMyUser
};
