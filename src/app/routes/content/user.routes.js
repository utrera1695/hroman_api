'use strict';

var express = require('express');
var UserController = require('../../controllers/user.controller');
var md_auth = require('../../../midleware/auth.midleware');
var api = express.Router();

api.post('/user', UserController.SaveUser);
api.post('/user/login', UserController.LoginUser);
api.put('/user', md_auth.ensureAuth, UserController.UpdateUser);
api.get('/users', md_auth.ensureAuth, UserController.ListUsers);
api.get('/user', md_auth.ensureAuth, UserController.ShowMyUser);
api.delete('/user', md_auth.ensureAuth, UserController.DeleteUser);
module.exports = api;
