'use strict';

const express = require('express');
const bodyParser = require('body-parser');
// const seeder = require('./src/config/seeders')
const multipart = require('connect-multiparty');
var cors = require('cors');

var app = express();

//CARGAR RUTAS
var routes = require('./src/app/routes/index');

app.use(multipart());
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(cors());

// seeder.UserSeeder();
// configurar cabeceras http
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header(
        'Access-Control-Allow-Headers',
        'Authorization,X-API-KEY,Origin,X-Rquested-Widht,Content-Type,Accept,Acces-Control-Allow-Request-Method'
    );
    res.header('Access-Control-Allow-Methods', '*');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

app.use('/api', routes.user);
app.use('/api', routes.informe);
app.use('/api', routes.chofer);

module.exports = app;
