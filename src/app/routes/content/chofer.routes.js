'use strict';

var express = require('express');
var ChoferController = require('../../controllers/chofer.controller');
var md_auth = require('../../../midleware/auth.midleware');
var api = express.Router();

api.post('/chofer', ChoferController.CreateChofer);
api.put('/chofer', md_auth.ensureAuth, ChoferController.UpdateChofer);
api.get('/chofer', md_auth.ensureAuth, ChoferController.GetAllChofer);
api.delete('/chofer', md_auth.ensureAuth, ChoferController.DeleteChofer);

module.exports = api;
