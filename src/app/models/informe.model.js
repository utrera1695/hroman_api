'use stric';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = Schema({
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    codigo: String,
    version: String,
    fecha_revision: String,

    elaborado_por: String,
    revisado_por: String,
    aprobado_por: String,
    fecha: String,
    nombre_del_ejecutor: String,
    lugar_de_trabajo: String,
    nombre_del_operador: String,
    equipo_camion: String,
    modelo: String,
    patente: String,

    tipo_mantencion: String,
    horometro_programado: String,
    horometro_real: String,
    km: String,
    mantencion_dentro_mm50h: String,
    mantencion_completa: String,

    aceites: [{ type: String }],
    filtros: [{ type: String }],
    elemntos_de_desgaste_cambiados: [
        {
            descripcion: String,
            cantidad: String
        }
    ],

    saca_fotografia: String,
    descripcion_del_trabajo: String,

    hora_icinio: String,
    hora_termino: String,
    total_horas: String,
    tiempo_de_traslado: String,
    tiempo_add_repuestos: String,
    servicios_externos: String,
    sin_personal_disp: String,
    observaciones: String,

    tabla_final: [
        {
            version: String,
            descripcion: String,
            fecha_aprobacion: String,
            elabora_aprueba: String
        }
    ],
    imagen: String
});

module.exports = mongoose.model('Informe', UserSchema);
